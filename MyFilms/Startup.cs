﻿using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using MyFilms.Models;
using Swashbuckle.AspNetCore.Swagger;
using System.Reflection;

namespace MyFilms
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();

            services.AddDbContext<MyFilmsContext>(options =>
                    options.UseInMemoryDatabase(Guid.NewGuid().ToString()));

            services.AddSwaggerGen(opt => {
                opt.SwaggerDoc("v1", new Info());
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSwagger();

            app.UseSwaggerUI(options =>
            {
                options.DocumentTitle = Assembly.GetExecutingAssembly().GetName().Name;

                options.ShowExtensions();
                options.DefaultModelRendering(ModelRendering.Example);

                options.SwaggerEndpoint("/swagger/v1/swagger.json", "FilmsApi");
            });

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                name: "default",
                template: "{controller=Defolt}/{action=Index}/{id?}");
            });
        }
    }
}
